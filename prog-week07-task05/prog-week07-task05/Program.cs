﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_week07_task05
{
    class Program
    {
        static void Main(string[] args)
        {
            //back up to call it return implies console writeline and link the variable
            var a = greeting("Amber");
            Console.WriteLine(a);
        }
        // Name the method (Leave the first 2 curly brackets then name method
        // Opening closing curly brackets
        // () the parameters in the parenthesis
        static string greeting(string name) 
            {
            return $"Hi {name} I hope you are having a great day!";
            }
    }
}
